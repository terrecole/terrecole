<?php
snippet('tete', [
  'navbarEstPrimaire' => true,
  'logo' => true,
  'sousTitre' => $page->sousTitre()->kirbytextinline(),
  'feuillesStyleAdditionnelles' => ['assets/css/rubrique.css']
]);
?>

<?= snippet('home/bandeau-principal', [
  'titre' => $page->titreSection1()->kirbytextinline(),
  'sousTitre' => $page->texteSection1()->kirbytextinline()
]) ?>

<div class="columns is-multiline section-columns container">
  <?= snippet('home/section', [
    'title' => $page->titreSection2()->kirbytextinline(),
    'texte' => $page->texteSection2()->kirbytextinline()
  ]) ?>

  <?= snippet('home/section', [
    'title' => $page->titreSection3()->kirbytextinline(),
    'texte' => $page->texteSection3()->kirbytextinline()
  ]) ?>
</div>

<?= snippet('home/partenariat') ?>
<?= snippet('home/contact') ?>

<?php
$pageActualites = $pages->find('actualites');

snippet('rubrique/section-rubrique', [
  'titre' => 'Nos dernières actualités',
  'titreEmoji' => '🗞',
  'rubriquePage' => $pageActualites,
  'rubriqueLienLibelle' => 'Accéder aux actualités'
])
?>

<?php
$pageProjet = $pages->find('projet');

snippet('rubrique/section-rubrique', [
  'titre' => 'Nos dernières ressources',
  'titreEmoji' => '📚',
  'rubriquePage' => $pageProjet,
  'rubriqueLienLibelle' => 'Accéder au projet'
])
?>

<?php snippet('pied') ?>
