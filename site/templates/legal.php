<?php snippet('tete') ?>

<section class="hero">
  <div class="hero-body container content">
    <h1>
      <?= $page->title()->esc() ?>
    </h1>
    <?= $page->contenu()->kirbytext() ?>
  </div>
</section>

<?php snippet('pied') ?>
