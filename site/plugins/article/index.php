<?php

// Reused from the Kirby Cookbook - https://getkirby.com/docs/cookbook/templating/different-blueprints-same-template

use Kirby\Cms\App as Kirby;

Kirby::plugin('terrecole/article', [
  'blueprints' => [
    'pages/article' => __DIR__ . '/blueprints/article.yml',
    'pages/article-sans-date' => __DIR__ . '/blueprints/article-sans-date.yml',
  ],
  'templates' => [
    'article' => __DIR__ . '/templates/article.php',
    'article-sans-date' => __DIR__ . '/templates/article.php',
  ],
]);
