<?php
snippet('tete', [
  'estQuaternaire' => true,
  'feuillesStyleAdditionnelles' => ['assets/css/article.css']
]);

$rubriquePage = $page->parent();
$libelles = $rubriquePage->libelles()->toObject();
?>
<section class="hero page-title-hero is-quaternary article-hero">
  <div class="hero-body">
    <a href="<?= $rubriquePage->url() ?>">
      <span aria-hidden="true">👈</span> <?= $libelles->get('lien-retour') ?>
    </a>
    <h1 class="title is-2 is-spaced mt-4">
      <?= $page->title()->kirbytextinline() ?>
    </h1>
    <p class="has-text-grey-dark mb-2 tags article-tags">
      <span class="has-text-weight-semibold">Rubrique :</span>
      <span class="tag article-rubrique is-medium <?= $rubriquePage->typeCouleur() ?>">
        <?= $libelles->get('nom-court') ?>
      </span>
    </p>
    <?= snippet('article/etiquettes', ['rubriquePage' => $rubriquePage]) ?>
    <?php if ($page->dateDePublication()->isNotEmpty()) : ?>
      <p class="has-text-grey-dark date-de-publication">
        <span class="has-text-weight-semibold">Publié le :</span>
        <?= snippet('date', ['date' => $page->dateDePublication()]) ?>
      </p>
    <?php endif ?>
  </div>
</section>

<div class="container article-container">
  <?php foreach ($page->contenu()->toBlocks() as $block) : ?>
    <?= $block ?>
  <?php endforeach ?>

  <?= snippet('article/navigation', [
    'precedentLibelle' => $libelles->get('lien-precedent'),
    'suivantLibelle' => $libelles->get('lien-suivant')
  ]) ?>
</div>

<?php snippet('pied') ?>
