<?php

Kirby::plugin('terrecole/link-block', [
  'blueprints' => [
    'blocks/link' => __DIR__ . '/blueprints/blocks/link.yml',
  ],
  'snippets' => [
    'blocks/link' => __DIR__ . '/snippets/blocks/link.php',
  ],
]);
