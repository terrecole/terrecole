<section class="section article-section article-section-link">
  <a href=<?= $block->lien()->escape() ?> <?php if ($block->titreDuLien()) : ?> title="<?= $block->titreDuLien()->escape() ?>" <?php endif ?> class="button is-primary is-outlined">
    <?php
    $hoteLien = parse_url($block->lien(), PHP_URL_HOST);
    if ($hoteLien === 'drive.google.com') :
    ?>
      <span aria-hidden="true" class="mr-1">📑</span>
    <?php endif ?>
    <?= $block->texteDuLien()->kirbytextinline() ?>
  </a>
</section>
