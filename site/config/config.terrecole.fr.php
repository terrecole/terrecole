<?php

require_once __DIR__ . '/../plugins/dotenv/global.php';

loadenv(['dir' => realpath(__DIR__ . '/../../')]);

return [
    'debug' => false,
    'sylvainjule.matomo.active' => true
];
