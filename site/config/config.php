<?php

require_once __DIR__ . '/../plugins/dotenv/global.php';

loadenv(['dir' => realpath(__DIR__ . '/../../')]);

return [
    'debug' => true,
    'sylvainjule.matomo.url' => 'https://statistiques.terrecole.fr',
    'sylvainjule.matomo.id' => '1',
    'sylvainjule.matomo.token' => env('MATOMO_TOKEN'),
    'sylvainjule.matomo.disableCookies' => true,
    'sylvainjule.matomo.active' => false // Enable this config if you want to debug Matomo while working locally
];
