<?php

use Kirby\Cms\Page as Page;

class ArticlePage extends Page
{
  public function voisins()
  {
    return $this->siblings()->sortBy('dateDePublication', 'desc');
  }
}
