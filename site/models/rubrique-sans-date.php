<?php

load([
  'RubriquePage' => 'rubrique.php',
], __DIR__);

class RubriqueSansDatePage extends RubriquePage
{
  public function articles($categorie = null, $ecole = null)
  {
    return $this->categorieArticles($categorie, $ecole);
  }
}
