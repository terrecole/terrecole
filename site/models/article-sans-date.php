<?php

use Kirby\Cms\Page as Page;

class ArticleSansDatePage extends Page
{
  public function voisins()
  {
    return $this->siblings();
  }
}
