<?php

use Kirby\Cms\App;
use Kirby\Cms\Page as Page;

class RubriquePage extends Page
{
  protected function categorieArticles($categorie = null, $ecole = null)
  {
    $categorie = $categorie ?? App::instance()->request()->query()->get('categorie');

    if (!$categorie) {
      return $this->children();
    }

    $ecole = $ecole ?? App::instance()->request()->query()->get('ecole');
    $ecole = $categorie === 'ecoles' ? $ecole : null;

    $aCategorie = fn ($article) => in_array($categorie, $article->categories()->split());

    $aEcole = function ($article, $ecole) {
      if (!$ecole) {
        return true;
      }

      $ecolesAssociees = $article->ecoles()->toPages();

      if (!$ecolesAssociees) {
        return false;
      }

      return $ecolesAssociees->has($ecole);
    };

    return $this->children()->filter(fn ($article) => $aCategorie($article) && $aEcole($article, $ecole));
  }

  public function articles($categorie = null, $ecole = null)
  {
    return $this->categorieArticles($categorie, $ecole)->sortBy('dateDePublication', 'desc');
  }
}
