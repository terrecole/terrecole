</main>

<footer class="footer mt-6">
  <div class="container">
    <h2 class="title is-5"><?= $site->title()->kirbytextinline() ?></h2>
    <div class="columns is-multiline">
      <?php
      $communications = $site->communications()->toStructure();
      if ($communications->isNotEmpty()) :
      ?>
        <div class="column is-half-tablet is-one-third-desktop">
          <h3 class="title is-6 mb-1">Nous suivre</h3>
          <ul>
            <?php foreach ($communications as $communication) : ?>
              <li>
                <a href="<?= $communication->lien() ?>" title="<?= $communication->titreDuLien() ?>">
                  <?= $communication->libelle() ?>
                </a>
              </li>
            <?php endforeach ?>
          </ul>
        </div>
      <?php endif ?>
      <div class="column is-half-tablet is-one-third-desktop">
        <h3 class="title is-6 mb-1">Plan du site</h3>
        <ul>
          <?php
          foreach ($site->children()->listed() as $page) :
          ?>
            <li>
              <a href="<?= $page->url() ?>"><?= $page->title()->kirbytextinline() ?></a>

              <?php if ($sousPages = $page->children()->listed()) : ?>
                <ul>
                  <?php foreach ($sousPages as $sousPage) : ?>
                    <li><a href="<?= $sousPage->url() ?>"><?= $sousPage->title()->kirbytextinline() ?></a></li>
                  <?php endforeach; ?>
                </ul>
              <?php endif; ?>
            </li>
          <?php endforeach ?>
        </ul>
      </div>
      <div class="column is is-half-tablet is-one-third-desktop">
        <p><?= $site->mentionsSources() ?></p>
      </div>
    </div>
    <?php
    $pagesLegales = $site->children()->template('legal');

    if ($pagesLegales) :
    ?>
      <ul class="pages-legales">
        <?php
        foreach ($pagesLegales as $pageLegale) : ?>
          <li><a href="<?= $pageLegale->url() ?>"><?= $pageLegale->title()->kirbytextinline() ?></a></li>
        <?php endforeach ?>
      </ul>
    <?php endif ?>
  </div>
</footer>

<?php
$feuillesScriptBase = [
  'assets/js/index.js',
  '@auto'
];
$feuillesScript = isset($feuillesScriptAdditionnelles) ? array_merge($feuillesScriptBase, $feuillesScriptAdditionnelles) : $feuillesScriptBase;
?>
<?= js($feuillesScript) ?>

<?php snippet('matomo'); ?>

</body>

</html>