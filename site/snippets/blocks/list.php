<?php
$blocPrecedentDansSection = $block->hasPrev() && ($block->prev()->type() === 'text' || $block->prev()->type() === 'heading');
if (!$blocPrecedentDansSection) :
?>
  <section class="section article-section">
  <?php endif ?>
  <?= $block->text() ?>
  <?php if (!$block->hasNext() || $block->next()->type() !== 'text') : ?>
  </section>
<?php endif ?>
