  <?php
  $blocPrecedentDansSection = $block->hasPrev() && ($block->prev()->type() === 'list' || $block->prev()->type() === 'heading');
  if (!$blocPrecedentDansSection) :
  ?>

    <section class="section article-section">
    <?php endif ?>
    <?= $block->text() ?>
    <?php if (!$block->hasNext() || $block->next()->type() !== 'list') : ?>
    </section>
  <?php endif ?>
