<section class="section article-section">
  <?php $niveauTitre = (int)str_replace('h', '', $block->level()->or('h2')) ?>
  <h<?= $niveauTitre ?> class="title is-<?= $niveauTitre - 2 ?>">
    <?= $block->text() ?>
  </h<?= $niveauTitre ?>>
  <?php
  $blocSuivantDansSection = $block->hasNext() && ($block->next()->type() === 'list' || $block->next()->type() === 'text');
  if (!$blocSuivantDansSection) :
  ?>
</section>
<?php endif ?>
