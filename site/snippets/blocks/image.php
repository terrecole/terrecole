<?php
$taillesVers = function ($tailles, $format) {
  $result = [];

  foreach ($tailles as $taille) {
    $result[$taille . 'w'] = ['width' => $taille, 'format' => $format];
  }

  return $result;
};

$TAILLES = [320, 640, 960, 1280];
$legende = $block->caption();
$image = $block->image()->toFile();
$imageDeSecours = $image->resize($TAILLES[0]);
$sourcesJpg = $image->srcset($taillesVers($TAILLES, 'jpg'));
$sourcesWebp = $image->srcset($taillesVers($TAILLES, 'webp'));
?>

<figure class="article-image">
  <picture>
    <source sizes="(max-width: 640px) 100vw, 640px" srcset="<?= $sourcesWebp ?>" type="image/webp" />
    <source sizes="(max-width: 640px) 100vw, 640px" srcset="<?= $sourcesJpg ?>" type="image/jpeg" />
    <img alt="<?= $block->alt()->esc() ?>" loading="lazy" src="<?= $imageDeSecours->url() ?>" height="<?= $image->height() ?>" width="<?= $image->width() ?>">
  </picture>

  <?php if ($legende->isNotEmpty()) : ?>
    <figcaption>
      <?= $legende ?>
    </figcaption>
  <?php endif ?>
</figure>
