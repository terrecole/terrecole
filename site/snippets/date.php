<?php
// Crédits Thomas Seignette - https://github.com/Terractiva/racines-de-resilience/blob/main/site/snippets/elements/time.php
?>

<time datetime="<?= $date->toDate('Y-m-d') ?>">
  <?php $formatter = new IntlDateFormatter('fr', IntlDateFormatter::LONG, IntlDateFormatter::NONE); ?>
  <?= $formatter->format($date->toDate()) ?>
</time>
