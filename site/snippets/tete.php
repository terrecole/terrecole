<?php
$estQuaternaire = $estQuaternaire ?? false;
$heroEstQuaternaire = $heroEstQuaternaire ?? $estQuaternaire;
$navbarEstPrimaire = $navbarEstPrimaire ?? false;
$navbarEstQuaternaire = !$navbarEstPrimaire && ($navbarEstQuaternaire ?? $estQuaternaire);
?>
<!DOCTYPE html>
<html lang="fr">

<head>

  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <meta name="referrer" content="origin-when-cross-origin">

  <?php
  /*
    Prevent image-scrapping tools for AI
    See https://neil-clarke.com/block-the-bots-that-feed-ai-models-by-scraping-your-website/
  */
  ?>
  <meta name="robots" content="noai, noimageai">

  <title><?= $site->title()->esc() ?> | <?= $page->title()->esc() ?></title>

  <?php snippet('tete-seo') ?>

  <?php
  $feuillesStyleBase = [
    'assets/css/index.css',
    '@auto'
  ];
  $feuillesStyle = isset($feuillesStyleAdditionnelles) ? array_merge($feuillesStyleBase, $feuillesStyleAdditionnelles) : $feuillesStyleBase;
  ?>
  <?= css($feuillesStyle) ?>

  <?php snippet('favicon') ?>
</head>

<body class="has-navbar-fixed-top">
  <?php snippet('navigation', [
    'estPrimaire' => $navbarEstPrimaire,
    'estQuaternaire' => $navbarEstQuaternaire,
  ]) ?>

  <main class="main">
    <?php
    if (isset($titre) || isset($logo)) :
      $titre = $titre ?? null;
      $logo = !$titre && $logo ?? null;
      $sousTitre = $sousTitre ?? null;

      snippet('bandeau', [
        'estQuaternaire' => $heroEstQuaternaire,
        'titre' => $titre,
        'logo' => $logo,
        'sousTitre' => $sousTitre
      ]);
    endif;
    ?>
