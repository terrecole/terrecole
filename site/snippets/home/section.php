<section class="section section-accueil container is-max-desktop column is-full is-half-desktop">
  <div class="card is-secondary is-lg is-full-height">
    <div class="card-content">
      <h2 class="title is-2"><?= $title ?></h2>
      <p class="block"><?= $texte ?></p>
    </div>
  </div>
</section>
