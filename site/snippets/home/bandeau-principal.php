<section class="hero bandeau-principal is-quaternary">
  <div class="hero-body container">
    <h2 class="title is-spaced is-2">
      <?= $titre ?>
    </h2>
    <p>
      <?= $sousTitre ?>
    </p>
    <svg class="picto-major" viewBox="0 0 401.59 219.45">
      <use xlink:href="/assets/pictos/e.svg#major"></use>
    </svg>
  </div>
</section>
