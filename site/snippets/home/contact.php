<section class="hero is-tertiary bandeau-contact">
  <div class="hero-body container">
    <svg class="picto-major" viewBox="0 0 240.61 206.52">
      <use xlink:href="/assets/pictos/partage.svg#major"></use>
    </svg>
    <p class="title" aria-hidden="true">👋</p>
    <p class="title">
      Si vous voulez en savoir plus sur ou participer au projet, écrivez-nous.
    </p>
    <a href="mailto:christophe.herlory@terrecole.fr" class="button is-primary is-medium">Nous contacter</a>
  </div>
</section>
