<section class="hero bandeau-partenariat is-quaternary">
  <div class="hero-body container">
    <p class="has-text-bold">Le projet Terrecole est soutenu par l’État dans le cadre du dispositif « Innovation dans la forme scolaire » de France 2030, opéré par le Groupe Caisse des Dépôts.</p>
    <img src="/assets/Logo_France_2030.svg" class="image" alt="" />
    <img src="/assets/Logo_du_Groupe_Caisse_des_Depots.svg" class="image" alt="" />
  </div>
</section>
