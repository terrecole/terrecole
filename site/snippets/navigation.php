<nav class="navbar is-fixed-top<?= e($estPrimaire, ' is-primary', e($estQuaternaire, ' is-quaternary')) ?>">
  <div class="navbar-brand">
    <a class="navbar-item" href="/" title="Accueil - <?= $site->title()->esc() ?>">
      <?php if ($logo = $estPrimaire ? $site->logoBlanc()->toFile() : $site->logo()->toFile()) : ?>
        <img src="<?= $logo->url() ?>" alt="<?= $site->title()->esc() ?>" class="logo">
      <?php else : ?>
        <?= $site->title()->esc() ?>
      <?php endif ?>
    </a>

    <button class="navbar-burger" title="Déplier le menu de navigation du site" aria-expanded="false" data-target="navbar-menu" onclick="onToggleMobileNavbarMenu(this)">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </button>
  </div>

  <?php
  $listedPages = $site->children()->listed();
  if ($listedPages->isNotEmpty()) :
  ?>
    <div class="navbar-menu" id="navbar-menu">
      <div class="navbar-end">
        <div class="navbar-item">
          <div class="buttons">
            <?php foreach ($listedPages as $page) : ?>
              <a class="button<?= e(!$page->isOpen(), e($estPrimaire, ' is-primary', e($estQuaternaire, ' is-quaternary', ' is-white'))) ?>" <?php e($page->isOpen(), 'aria-current="page"') ?> href="<?= $page->url() ?>">
                <?= $page->title()->kirbytextinline() ?>
              </a>
            <?php endforeach ?>
          </div>
        </div>
      </div>
    </div>
  <?php endif ?>
</nav>
