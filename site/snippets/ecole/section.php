<section class="column is-6-tablet is-narrow-desktop">
  <div class="card is-secondary is-lg is-full-height">
    <div class="card-content">
      <h2 class="title is-2"><?= $title ?></h2>
      <?= $slot ?>
    </div>
  </div>
</section>
