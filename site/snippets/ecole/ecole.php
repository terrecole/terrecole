<?php

use Kirby\Cms\HTML as HTML;

snippet('tete', [
  'titre' => $page->title()->kirbytextinline(),
  'sousTitre' => $page->sousTitre()->kirbytextinline(),
  'feuillesStyleAdditionnelles' => ['assets/css/ecole.css', 'assets/css/rubrique.css']
]);
?>
<div class="container">
  <section class="section">
    <h2 class="title is-2"><?= $page->titrePresentation()->kirbytextinline() ?></h2>
    <p class="block"><?= $page->textePresentation()->kirbytextinline() ?></p>
  </section>

  <?php
  $blocsEquipe = $page->blocsEquipe()->toStructure();

  if ($blocsEquipe->isNotEmpty()) :
  ?>
    <section class="section">
      <h2 class="title is-2"><?= $page->titreEquipe()->kirbytextinline() ?></h2>
      <ul class="columns is-multiline">
        <?php foreach ($blocsEquipe as $blocEquipe) : ?>
          <li class="column is-6-tablet is-4-desktop">
            <h3 class="title is-4"><?= $blocEquipe->titre()->kirbytextinline() ?></h3>
            <p class="block"><?= $blocEquipe->description()->kirbytextinline() ?></p>
          </li>
        <?php endforeach ?>
      </ul>
    </section>
  <?php endif ?>

  <div class="section columns is-centered">
    <?php snippet('ecole/section', ['title' => $page->titreHoraires()->kirbytextinline()], slots: true) ?>
    <ul class="horaires">
      <?php
      $jours = array('lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche');

      foreach ($jours as $jour) :
        $horaires = $page->content()->get('horaires' . $jour)->toObject();
      ?>
        <li>
          <strong class="mr-6"><?= $jour ?></strong>
          <span>
            <?php if ($horaires->estOuvert()->toBool()) : ?>
              <?= snippet('heure', ['heure' => $horaires->heureOuverture()]) ?>
              -
              <?= snippet('heure', ['heure' => $horaires->heureFermeture()]) ?>
            <?php else : ?>
              fermé
            <?php endif ?>
          </span>
        </li>
      <?php endforeach ?>
    </ul>
    <?php endsnippet() ?>

    <?php snippet('ecole/section', ['title' => $page->titreContact()->kirbytextinline()], slots: true) ?>
    <address>
      <ul>
        <li class="contact-adresse">
          <strong>adresse :</strong> <?= $page->adresseContact()->kirbytextinline() ?>
        </li>
        <li class="contact-telephone">
          <strong>téléphone :</strong> <?= Html::tel($page->telephoneContact()) ?>
        </li>
        <li class="contact-email">
          <strong>adresse e-mail :</strong> <?= Html::email($page->emailContact()) ?>
        </li>
      </ul>
    </address>
    <?php endsnippet() ?>
  </div>

  <?php
  $pageActualites = $pages->find('actualites');

  snippet('rubrique/section-rubrique', [
    'titre' => $page->titreActualites()->kirbytextinline(),
    'titreEmoji' => '🗞',
    'rubriquePage' => $pageActualites,
    'rubriqueLienLibelle' => 'Accéder aux actualités',
    'categorie' => $categorie,
    'ecole' => $ecole ?? null
  ])
  ?>

</div>
<?php snippet('pied') ?>
