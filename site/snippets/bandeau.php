<section class="hero page-title-hero<?= e($estQuaternaire, ' is-quaternary') ?>">
  <div class="hero-body">
    <h1 class="title is-spaced<?= e($titre, ' is-1 has-text-centered') ?>">
      <?php if ($titre) : ?>
        <?= $titre ?>
      <?php elseif ($logo) : ?>
        <img src="<?= $site->logo()->toFile()->url() ?>" alt="<?= $site->title()->esc() ?>">
      <?php
      endif
      ?>
    </h1>
    <?php if ($sousTitre) : ?>
      <p class="subtitle is-3 has-text-centered">
        <?= $sousTitre ?>
      </p>
    <?php endif ?>
  </div>
</section>
