<?php
$voisinsRubrique = $page->voisins();
$articleSuivant = $page->next($voisinsRubrique);
$articlePrecedent = $page->prev($voisinsRubrique);

if ($articleSuivant || $articlePrecedent) :
?>
  <section class="section article-section navigation-article-section">
    <?php if ($articlePrecedent) : ?>
      <a href="<?= $articlePrecedent->url() ?>" rel="prev">
        <span aria-hidden="true">⏮</span> <?= $precedentLibelle ?>
      </a>
    <?php
    endif;
    if ($articleSuivant) :
    ?>
      <a href="<?= $articleSuivant->url() ?>" rel="next" class="lien-article-suivant">
        <?= $suivantLibelle ?> <span aria-hidden="true">⏭</span>
      </a>
    <?php endif ?>
  </section>
<?php endif ?>
