<?php
$etiquettesSansLien = $etiquettesSansLien ?? false;

if ($page->categories()->isNotEmpty()) :
?>
  <?php
  $categorieOptions = $page->blueprint()->field('categories')['options'];
  $categories = $page->categories()->split();
  ?>
  <p class="has-text-grey-dark mb-2 tags article-tags">
    <span class="has-text-weight-semibold">Catégorie<?= e(sizeof($categories) > 1, 's') ?> :</span>
    <?php foreach ($categories as $categorie) : ?>
      <span class="article-category tag is-medium">
        <?php if (!$etiquettesSansLien) : ?>
          <a href=" <?= $rubriquePage->url() ?>?categorie=<?= $categorie ?>">
          <?php endif ?>
          <?= $categorieOptions[$categorie] ?? $categorie ?>
          <?php if (!$etiquettesSansLien) : ?>
          </a>
        <?php endif ?>
      </span>
    <?php endforeach ?>
  </p>

  <?php
  $aEcole = in_array('ecoles', $categories) && $page->ecoles()->isNotEmpty();
  if ($aEcole) :
  ?>
    <p class="has-text-grey-dark mb-2 tags article-tags">
      <span class="has-text-weight-semibold">École :</span>

      <?php foreach ($page->ecoles()->toPages() as $ecole) : ?>
        <span class="article-ecole tag is-medium">
          <?php if (!$etiquettesSansLien) : ?>
            <a href="<?= $ecole->url() ?>">
            <?php endif ?>
            <?= $ecole->title() ?>
            <?php if (!$etiquettesSansLien) : ?>
            </a>
          <?php endif ?>
        </span>
      <?php endforeach ?>
    </p>
  <?php endif ?>
<?php endif ?>