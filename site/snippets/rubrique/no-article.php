<?php $categorieSelectionnee = $categorieSelectionnee ?? null ?>
<p class="notification is-info is-light block">
  <?= $categorieSelectionnee ? $page->aucunArticleFiltre()->kirbytextinline() : $page->aucunArticle()->kirbytextinline() ?>
</p>
