<section class="section rubrique-section container">
  <h2 class="title is-2">
    <span aria-hidden="true"><?= $titreEmoji ?></span> <?= $titre ?>
  </h2>
  <?php
  $articles = $rubriquePage->articles($categorie ?? null, $ecole ?? null);
  if ($articles->isNotEmpty()) :
  ?>
    <ul class="columns is-multiline article-list">
      <?php
      foreach ($articles->limit(3) as $article) :
        snippet('rubrique/article-card', [
          'article' => $article,
          'lienLibelle' => $rubriquePage->libelles()->toObject()->get('lien-lire'),
          'niveauTitre' => 3
        ]);
      endforeach
      ?>
    </ul>
  <?php else :
    snippet('rubrique/no-article', ['page' => $rubriquePage]);
  endif
  ?>

  <?php
  if ($articles->count() > 3) :
    $rubriqueLien = $rubriquePage->url();
    $categorie = $categorie ?? null;

    if ($categorie) {
      $rubriqueLien = $rubriqueLien . "?categorie=" . urlencode($categorie);
      $ecole = $ecole ?? null;

      if ($categorie === 'ecoles' && $ecole) {
        $rubriqueLien = $rubriqueLien . "&ecole=" . urlencode($ecole->id());
      }
    }
  ?>
    <a href="<?= $rubriqueLien ?>" class="button is-primary is-medium rubrique-link">
      <?= $rubriqueLienLibelle ?><span aria-hidden="true"> 👉</span>
    </a>
  <?php endif ?>
</section>
