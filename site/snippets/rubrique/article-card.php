<?php $niveauTitre = $niveauTitre ?? 2 ?>

<li class="column is-6-tablet is-4-desktop article">
  <div class="card is-full-height <?= $article->parent()->typeCouleur() ?>">
    <div class="card-content">
      <h<?= $niveauTitre ?> class="title is-5 mb-0">
        <?= $article->title()->kirbytextinline() ?>
      </h<?= $niveauTitre ?>>
      <?php if ($article->dateDePublication()->isNotEmpty()) : ?>
        <p class="subtitle is-6 mt-2 is-italic">
          Le <?= snippet('date', ['date' => $article->dateDePublication()]) ?>
        </p>
      <?php
      endif;
      if ($article->categories()->isNotEmpty()) :
      ?>
        <?=
        snippet('article/etiquettes', [
          'page' => $article,
          'etiquettesSansLien' => true,
        ]) ?>
      <?php endif ?>
    </div>

    <div class="card-content">
      <?php if ($article->description()->isNotEmpty()) : ?>
        <p class="mb-4">
          <?= $article->description()->kirbytextinline() ?>
        </p>
      <?php endif ?>
      <a class="title is-5 mt-auto" href="<?= $article->url() ?>">
        <?= $lienLibelle ?>
      </a>
    </div>
  </div>
</li>
