<?php
$pagination = $collectionDePages->pagination();
if ($pagination->pages() > 1) :
  $estSurPremierePage = $pagination->page() === 1;
  $estSurDernierePage = $pagination->page() === $pagination->pages();
  $estApresPremieresPages = $pagination->pages() > 5 && $pagination->page() > 3;
  $estAvantDernieresPages = $pagination->pages() > 5 && $pagination->page() < $pagination->pages() - 2;
?>
  <nav class="pagination rubrique-section-action is-centered mt-5" role="navigation" aria-label="<?= $paginationLibelle ?>">
    <ul class="pagination-list">
      <!-- Page précédente -->
      <li>
        <a class="pagination-previous<?= e(!$pagination->hasPrevPage(), ' is-disabled') ?>" <?= e($pagination->hasPrevPage(), "href=\"{$pagination->prevPageURL()}\"", 'aria-disabled="true" role="link"') ?>>
          Page précédente
        </a>
      </li>

      <!-- Première page -->
      <li>
        <a class="pagination-link<?= e($estSurPremierePage, ' is-current') ?>" title="Page 1" <?= e($estSurPremierePage, ' aria-current="page"') ?> href="<?= $pagination->pageURL(1) ?>">
          1
        </a>
      </li>

      <!-- Ellipse entre la première page et la page actuelle si on est au-delà de la deuxième page -->
      <?php
      if ($estApresPremieresPages) :
      ?>
        <li><span class="pagination-ellipsis">&hellip;</span></li>
      <?php endif ?>

      <?php
      if ($pagination->pages() > 2) :
        # On affiche la page d'avant et la page d'après
        $debutIntervalle = max($pagination->page() - 1, 2);
        $finIntervalle = $debutIntervalle + 2;

        while ($finIntervalle >= $pagination->pages()) :
          $finIntervalle--;
          if ($debutIntervalle > 2) :
            $debutIntervalle--;
          endif;
        endwhile;

        foreach (range($debutIntervalle, $finIntervalle) as $pageDePagination) :
          $estPageActuelle = $pagination->page() === $pageDePagination
      ?>
          <li>
            <a class="pagination-link<?= e($estPageActuelle, ' is-current') ?>" title="Page <?= $pageDePagination ?>" <?= e($estPageActuelle, ' aria-current="page"') ?> href="<?= $pagination->pageURL($pageDePagination) ?>">
              <?= $pageDePagination ?>
            </a>
          </li>
      <?php
        endforeach;
      endif;
      ?>

      <!-- Ellipse entre la dernière page et la page actuelle si on est avant l'avant dernière page -->
      <?php
      if ($estAvantDernieresPages) :
      ?>
        <li><span class="pagination-ellipsis">&hellip;</span></li>
      <?php endif ?>

      <!-- Dernière page -->
      <li>
        <a class="pagination-link<?= e($estSurDernierePage, ' is-current') ?>" title="Page <?= $pagination->pages() ?>" <?= e($estSurDernierePage, ' aria-current="page"') ?> href="<?= $pagination->pageURL($pagination->pages()) ?>">
          <?= $pagination->pages() ?>
        </a>
      </li>

      <!-- Page suivante -->
      <li>
        <a class="pagination-next<?= e(!$pagination->hasNextPage(), ' is-disabled') ?>" <?= e($pagination->hasNextPage(), "href=\"{$pagination->nextPageURL()}\"", 'aria-disabled="true" role="link"') ?>>
          Page suivante
        </a>
      </li>
    </ul>
  </nav>
<?php endif ?>
