<form class="rubrique-section-action field is-grouped mb-6" action="<?= $page->url() ?>">
  <div class="field">
    <label class="label" for="categorie">Filtrer par catégorie</label>
    <div class="control">
      <div class="select">
        <select id="categorie" name="categorie">
          <option value="">Toutes les catégories</option>
          <?php foreach ($categorieOptions as $valeur => $libelle) : ?>
            <option value="<?= $valeur ?>" <?php e($categorieSelectionnee === $valeur, 'selected') ?>><?= $libelle ?></option>
          <?php endforeach ?>
        </select>
      </div>
    </div>
  </div>

  <div class="field" id="filtre-ecole" <?= $categorieSelectionnee !== 'ecoles' ? 'hidden' : '' ?>>
    <label class="label" for="ecole">Filtrer par école</label>
    <div class="control">
      <div class="select">
        <select id="ecole" name="ecole" <?= $categorieSelectionnee !== 'ecoles' ? 'disabled' : '' ?>>
          <option value="">Toutes les écoles</option>
          <?php foreach ($site->children()->template('rubrique-ecoles')->children() as $pageEcole) : ?>
            <option value="<?= $pageEcole->id() ?>" <?php e($ecoleSelectionnee === $pageEcole->id(), 'selected') ?>><?= $pageEcole->title() ?></option>
          <?php endforeach ?>
        </select>
      </div>
    </div>
  </div>

  <div class="field">
    <div class="control">
      <button class="button is-primary">Valider</button>
    </div>
  </div>
</form>
