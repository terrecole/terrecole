<?php
snippet('tete', [
  'titre' => $page->title()->kirbytextinline(),
  'sousTitre' => $page->sousTitre()->kirbytextinline(),
  'feuillesStyleAdditionnelles' => ['assets/css/rubrique.css']
]);

$libelles = $page->libelles()->toObject();
$aArticles = $page->articles()->isNotEmpty();
$categories = $page->children()->first()->blueprint()->field('categories');
$categorieSelectionnee = $kirby->request()->query()->get('categorie');
$categorieSelectionnee = $categorieSelectionnee ? urldecode($categorieSelectionnee) : null;

?>
<div class="container">
  <section class="section">
    <?php
    if ($categories && ($aArticles || $categorieSelectionnee)) :
      $ecoleSelectionnee = $kirby->request()->query()->get('ecole');
      $ecoleSelectionnee = $ecoleSelectionnee ? urldecode($ecoleSelectionnee) : null;

    ?>
      <?= snippet('rubrique/filtre-rubrique', [
        'categorieOptions' => $categories['options'],
        'categorieSelectionnee' => $categorieSelectionnee,
        'ecoleSelectionnee' => $ecoleSelectionnee
      ]) ?>
    <?php
    endif;
    if ($aArticles) :
    ?>

      <ul class="columns is-multiline">
        <?php
        $taillePage = 12;
        $pageArticles = $page->articles()->paginate($taillePage);
        foreach ($pageArticles as $article) :
          snippet('rubrique/article-card', [
            'article' => $article,
            'lienLibelle' => $libelles->get('lien-lire')
          ]);
        endforeach
        ?>
      </ul>

      <?= snippet('rubrique/pagination', [
        'collectionDePages' => $pageArticles,
        'paginationLibelle' => $libelles->get('a11y-pagination'),
        'taillePage' => $taillePage
      ]) ?>
    <?php
    else :
      snippet('rubrique/no-article', [
        'categorieSelectionnee' => $categorieSelectionnee
      ]);
    endif
    ?>
  </section>
</div>
<?php snippet('pied', [
  'feuillesScriptAdditionnelles' => ['assets/js/rubrique.js']
]) ?>