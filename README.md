# Terrecole

Le projet [Terrecole](https://terrecole.fr/), lauréat de l'appel à manifestation d'intérêt **Innovation dans la forme scolaire** de [France 2030](https://www.gouvernement.fr/france-2030), vise à mettre en place une démarche d'ingénierie territoriale rurale associant les acteurs de l'École et leurs partenaires afin de renouveler le Parcours de l'Enfant depuis les 1000 premiers jours jusqu'à la sortie du collège. Le projet se déploie sur un territoire rural de Sud Touraine. 🏫

Ce dépôt contient le code source du site Web de Terrecole : https://terrecole.fr/.

## ⛓ Dépendances

Pour démarrer ce projet chez vous, vous avez besoin de :

-   [PHP 8](https://www.php.net/manual/fr/install.php), un langage _à l'ancienne_ qui exécute facilement du code pour produire des pages Web depuis un serveur, et qui a le mérite de faire ce qu'on lui demande ;
-   [bun.sh](https://bun.sh/docs/installation), une alternative à Node.js qui permet d'exécuter moins péniblement du JavaScript sur votre ordinateur et qui [tente](https://bun.sh/docs/cli/run) [de](https://bun.sh/docs/cli/install) [sauver](https://bun.sh/docs/install/cache) [votre ordinateur](https://bun.sh/docs/bundler)
-   [IndigoStack](https://indigostack.app/), un outil ~~un peu magique~~ qui permet de facilement mettre en place une configuration de serveur Web pour travailler en local. Cet outil est uniquement disponible sur macOS 🍏 (voir plus bas).

Et c'est à peu près tout... Mais déjà beaucoup !

## ⚗️ Technologie

Le projet est développé à l'aide du système de gestion de contenu (_[CMS](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_gestion_de_contenu)_) [Kirby](https://getkirby.com/) avec sa [version 3](https://v39.getkirby.com/docs/guide).

Les scripts du site sont écrits en JavaScript natif. Les styles du site sont écrits en [SCSS](https://www.sass-lang.com/) et utilisent le cadriciel CSS [Bulma](https://bulma.io/).

## 🧑‍🔧 Pour commencer

### Cloner le projet

Dans le terminal de votre choix :

```bash
git clone git@gitlab.com:terrecole/terrecole.git
cd terrecole
git submodule update --init
```

La dernière commande permet d’initialiser les plugins Kirby de la communauté installé en tant que sous-module Git.

### Démarrer le serveur avec IndigoStack (macOS 🍏)

-   Ouvrir IndigoStack
-   Ajouter la configuration IndigoStack du projet : Fichier > Ouvrir > Sélectionner le dossier `stack.indigostack` à la racine de ce dossier
-   Pointer les noms de domaine vers le dossier du dépôt dans la configuration Apache (Terrecole > Apache > terrecole.localhost > Edit site... > Root > Sélectionner le dossier du dépôt).
-   Démarrer la configuration IndigoStack

Le site est désormais accessible sur https://terrecole.localhost/. 🚀

### Démarrer le serveur sur un autre système d'exploitation

Actuellement, les développeurs du projet travaillent tous sur macOS et ont fait le choix de la facilité d'utiliser IndigoStack.

Pour démarrer le serveur sur un autre système d'exploitation, le projet requiert l'installation du serveur HTTP [Apache](https://httpd.apache.org/). La solution la plus courante est d'utiliser [LAMP](https://doc.ubuntu-fr.org/lamp) sur Linux ou [WAMP](https://www.wampserver.com/) sur Windows.

📭 Cette section est ouverte à contribution !

### Développement JS et SCSS

Pour tout ce qui touche à des outils Node.js (validation et formatage du code, optimisation d'images, etc.) ou si vous apportez des modifications aux fichiers de script JavaScript ou de style SCSS, il vous faudra installer les dépendances et exécuter la commande associée dans le fichier `package.json`

Liste des commandes :

-   `bun install` : Installe les dépendances `node` 📦. À exécuter à la première utilisation ou après une mise à jour du fichier `package.json` ;
-   `bun run dev` : Reconstruit les fichiers CSS pour le développement. Cette commande produit des fichiers non-minifiés et les reconstruit à chaque nouveau changement ;
-   `bun run lint` : Valide la qualité et le _formattage_ du code ;
-   `bun run build` : Reconstruit les fichiers CSS pour la production. Cette commande produit des fichiers minifiés et est à exécuter avant de pousser des changements sur une branche ;

## 🤜 Contribuer

Le projet respecte des conventions de codage qui peuvent être validées en exécutant `bun run lint`. 💅

La validation de la qualité du code peut être automatiquement exécutée en installant un _hook_ Git avec l'outil `simple-git-hooks` :

```bash
bun run simple-git-hooks
```

Le développement en cours s'effectue sur une branche dédiée qui devra être fusionnée sur la branche `dev` une fois la fonctionnalité ou la tâche finalisée. Cette branche correspond à un environnement de validation déployé sur https://en-fermentation.terrecole.fr/.

Les réalisations sur sont projets sont documentés à destination de l'équipe de Terrecole dans le fichier `JOURNALDEBORD.md`.

### Kirby

L'essentiel des fichiers du projet se trouvent dans le dossier `site`, en dehors des scripts JS et styles SCSS présents dans les dossiers `assets/css` et `assets/js`.

Nous ne pourrons pas être plus exhaustif que la [documentation officielle de Kirby 3.9](https://v39.getkirby.com/docs/guide), mais pour une prise en main rapide :

-   `blueprints` : Contient des fichiers YML qui décrivent la structure des données de chaque page et comment ces données sont présentées et modifiées dans le panneau d'administration ;
-   `models` : Contient des classes PHP qui surchargent la classe `Page` Kirby pour définir des comportements spécifiques de certaines pages ;
-   `plugins` : Contient les dépendances de plugins Kirby de la communauté ou des bouts de code utiles pour étendre les fonctionnalités de Kirby (voir `article`, `link-block` ou `rubrique-template` par exemple) ;
-   `snippets` : Contient des morceaux de PHP/HTML réutilisés dans plusieurs pages ou isolés pour simplifier la lecture du code ;
-   `templates` : Contient le code PHP/HTML qui définit la sémantique de chaque page.

## 📤 Déployer

Le site de Terrecole est hébergé chez [Scaleway](http://scaleway.com/).

Tout déploiement, sur l'environnement de validation comme sur le domaine final, requiert une action humaine. Il n'y a pas de déploiement automatique. 🤖

Afin de pouvoir déployer sur un environnement, il vous faudra disposer d'une clé SSH disponible sur notre serveur Scaleway et ajouter l'hôte SSH du serveur dans votre configuration SSH `~/.ssh/config` sous le nom `terrecole.fr`.

-   Déployer sur l'environnement de validation :

    ```bash
    ssh terrecole.fr
    cd en-fermentation.terrecole.fr
    git checkout dev
    git pull
    ```

-   Déployer sur l'environnement de production :
    ```bash
    ssh terrecole.fr
    cd public_html
    git checkout main
    git pull
    ```

Sur l'environnement de production, les changements effectués depuis le panneau d'administration doivent être ajoutés à la branche `main` avant la fusion de la branche `dev` dans celle-ci :

```bash
ssh terrecole.fr
cd public_html
git add content
git commit -m "fonctionnalite(*): met à jour le contenu"
git push
```

## 🧘 On développe comment ?

En raison des enjeux sociaux et environnementaux actuels, le site de Terrecole se veut le plus simple, le plus léger et le plus accessible possible, tout en maintenant un haut niveau de maintenabilité aussi bien pour les développeurs que pour les rédacteurs.

## 🐞 Signaler un bug

Pour signaler un bug, merci de créer un ticket sur le dépôt GitLab : [Créer un ticket](https://gitlab.com/terrecole/terrecole/-/issues/new).

## 🧑‍💻 Auteurs

-   @mlbiche - [🤝 Savoir qui c'est ?](http://mlbiche.fr/)
-   @tseignette - [🤝 Savoir qui c'est ?](https://www.linkedin.com/in/thomas-seignette/)
