Title: Ecole d'Orbigny

----

Soustitre: Quelques informations sur l'école d'Orbigny

----

Titrepresentation: Ecole publique d'Orbigny

----

Textepresentation:

Ecole Publique
(link: https://www.education.gouv.fr/annuaire/37460/orbigny/ecole/0370935r/ecole-elementaire.html text: Académie d'Orléans-Tours)
(link: https://www.service-public.fr/particuliers/vosdroits/F31952 text: Zone B)
37 élèves

----

Titreequipe: Equipe pédagogique

----

Blocsequipe:

- 
  titre: Directrice
  description: Céline Bibard

----

Titrehoraires: Horaires de l'école

----

Horaireslundi:

estouvert: 'true'
heureouverture: 08:50:00
heurefermeture: 16:20:00

----

Horairesmardi:

estouvert: 'true'
heureouverture: 08:50:00
heurefermeture: 16:20:00

----

Horairesmercredi:

estouvert: 'false'
heureouverture: ""
heurefermeture: ""

----

Horairesjeudi:

estouvert: 'true'
heureouverture: 08:50:00
heurefermeture: 16:20:00

----

Horairesvendredi:

estouvert: 'true'
heureouverture: 08:50:00
heurefermeture: 16:20:00

----

Horairessamedi:

estouvert: 'false'
heureouverture: ""
heurefermeture: ""

----

Horairesdimanche:

estouvert: 'false'
heureouverture: ""
heurefermeture: ""

----

Titrecontact: Coordonnées et contact

----

Adressecontact: 10 rue Jeanne d'Arc 37460 Orbigny

----

Telephonecontact: 02 47 94 37 57

----

Emailcontact: ec-orbigny@ac-orleans-tours.fr

----

Description: Quelques informations sur l'école d'Orbigny

----

Uuid: g0hMNCiG8wu1gYsh