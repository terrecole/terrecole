Title: Signature de l'accord de partenariat avec la Communauté de communes Loches Sud Touraine

----

Datedepublication: 2023-12-20

----

Description: Signature de l'accord de partenariat avec la Communauté de communes Loches Sud Touraine

----

Contenu: [{"content":{"text":"<p>Vendredi 15 décembre 2023 l'Association des maires de l'ancien canton de Montrésor, la Communauté de communes LOCHES SUD TOURAINE et la SCIC TERRECOLE ont signé un accord de partenariat dans le cadre du projet Terrecole.</p><p>L'objectif de cet accord est d'engager la collaboration et les actions mutualisées entre les parties autour notamment de l'éducation, de la jeunesse, la petite enfance et la parentalité.</p><p>Nous remercions la Communauté de communes LOCHES SUD TOURAINE pour sa confiance sur le projet TERRECOLE !</p>"},"id":"7933bf31-6824-4783-9d1b-ba2ee824cdf9","isHidden":false,"type":"text"},{"content":{"location":"kirby","image":["file://G4GEgEwrm0jIBIeN"],"src":"","alt":"","caption":"","link":"","ratio":"","crop":"false"},"id":"ede38f71-e134-4811-a0e4-688030022b10","isHidden":false,"type":"image"}]

----

Uuid: O073MEa1F91C0kJ1