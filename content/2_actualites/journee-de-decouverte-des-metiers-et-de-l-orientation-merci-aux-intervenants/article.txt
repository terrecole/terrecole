Title: Journée de découverte des métiers et de l’orientation : merci aux intervenants !

----

Datedepublication: 2023-05-12

----

Description: Journée de découverte des métiers et de l’orientation au collège de Montrésor : merci aux intervenants !

----

Contenu: [{"content":{"text":"<p>Plus de 40 professionnels du Val d'Indrois et de la Région Centre-Val de Loire étaient présents hier lors de la première édition de la journée de découverte des métiers et de l'orientation.</p>"},"id":"c59d8f5b-ff3d-4c1f-9697-107c81e1b720","isHidden":false,"type":"text"},{"content":{"location":"kirby","image":["file://5Y3ev2mrZPMGWDep"],"src":"","alt":"","caption":"","link":"","ratio":"","crop":"false"},"id":"7a0c4815-d7c1-489a-9efa-cee610347d14","isHidden":false,"type":"image"},{"content":{"text":"<p>Terrecole remercie chaleureusement les intervenants qui ont consacré une journée pour échanger avec nos 250 élèves du CE2 à la troisième.</p>"},"id":"362928b0-10e1-4979-84af-e9a8bca20af4","isHidden":false,"type":"text"},{"content":{"location":"kirby","image":["file://MCf0aIgMaAqe7rmr"],"src":"","alt":"","caption":"","link":"","ratio":"","crop":"false"},"id":"0525f78b-ea48-42c5-ba8b-feb90f6ba695","isHidden":false,"type":"image"},{"content":{"text":"<p>L'éveil professionnel prend tout son sens auprès des élèves qui ont pu notamment poser toutes leurs questions, avoir une première approche des gestes métiers, toucher le matériel des intervenants.</p>"},"id":"42db36ba-ee8a-4cd0-a6e0-a489ef6a4ad6","isHidden":false,"type":"text"},{"content":{"location":"kirby","image":["file://BtR8fqrmThlPDfM8"],"src":"","alt":"","caption":"","link":"","ratio":"","crop":"false"},"id":"76d58795-abf2-48d0-aacf-a079db39233c","isHidden":false,"type":"image"},{"content":{"text":"<p>Cette journée marque un temps particulier dans le travail conduit par les équipes pédagogiques tout au long de l'année pour développer les compétences à s'orienter des élèves !</p>"},"id":"0e62209c-185b-4a18-97da-849eb0992e6e","isHidden":false,"type":"text"},{"content":{"location":"kirby","image":["file://tO8T7E4ALw8Wvjoh"],"src":"","alt":"","caption":"","link":"","ratio":"","crop":"false"},"id":"e88dc77c-4d13-46d1-8ede-66dffbdd54ad","isHidden":false,"type":"image"},{"content":{"location":"kirby","image":["file://W7NEDPhax8YteTxA"],"src":"","alt":"","caption":"","link":"","ratio":"","crop":"false"},"id":"56261849-144e-4ef2-9564-d2490cb19e2d","isHidden":false,"type":"image"},{"content":{"location":"kirby","image":["file://pZwB5FlfBknZbMSm"],"src":"","alt":"","caption":"","link":"","ratio":"","crop":"false"},"id":"294f0154-c68f-4b09-8f55-23e75821f5ea","isHidden":false,"type":"image"},{"content":{"location":"kirby","image":["file://yHwz1Q6GOHsyPYTS"],"src":"","alt":"","caption":"","link":"","ratio":"","crop":"false"},"id":"8e6decb3-c8e1-4f03-a8b6-0fbb9f328663","isHidden":false,"type":"image"}]

----

Uuid: NhmkcRsCVUhwFo0y