onToggleMobileNavbarMenu = el => {
  const navbarMenu = el.dataset.target
  const $navbarMenu = document.getElementById(navbarMenu)

  el.classList.toggle('is-active')
  el.setAttribute('aria-expanded', el.getAttribute('aria-expanded') === 'false')
  $navbarMenu.classList.toggle('is-active')
}
