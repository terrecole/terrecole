/** @type {HTMLDivElement | null} */
let conteneurFiltreEcole = null

/** @type {HTMLSelectElement | null} */
let filtreEcole = null

/**
 * @param {Event} e
 */
function onFiltreCategorieChange(e) {
  if (!conteneurFiltreEcole || !filtreEcole) {
    return
  }

  const categorieSelectionnee = /** @type {string} */ (e.target.value)

  if (categorieSelectionnee === 'ecoles') {
    conteneurFiltreEcole.removeAttribute('hidden')
    filtreEcole.removeAttribute('disabled')
  } else {
    conteneurFiltreEcole.setAttribute('hidden', '')
    filtreEcole.setAttribute('disabled', '')
  }
}

function initFiltres() {
  const filtreCategorie = /** @type {HTMLSelectElement | null} */ (
    document.querySelector('[name="categorie"]')
  )

  if (!filtreCategorie) {
    return
  }

  conteneurFiltreEcole = document.querySelector('#filtre-ecole')

  if (!conteneurFiltreEcole) {
    return
  }

  filtreEcole = conteneurFiltreEcole.querySelector('[name="ecole"]')

  if (!filtreEcole) {
    return
  }

  filtreCategorie.addEventListener('change', onFiltreCategorieChange)
}

initFiltres()
