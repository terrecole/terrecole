# 🧭 Journal de bord

Ce journal de bord a pour vocation de faire état de l'avancement du projet. Il ne s'agit pas de notes de version ni d'une [historique de _commits_ d'outils de versionnage](https://github.com/conventional-changelog/standard-version), notamment en raison des différences suivantes :

-   il rend transparent aux autres parties prenantes du projet le travail effectué par l'équipe technique, dans un soucis de vulgarisation et de transparence ;
-   il permet à l'équipe technique de prendre le temps de digérer le travail effectué ;
-   il tient une compatibilité des crédits consommés sur le projet ;
-   il permet de suivre l'évolution de la motivation des troupes ;
-   il peut servir de prises de notes des difficultés rencontrées et de leurs solutions employées, à la manière d'un journal scientifique.

À partir du 1er décembre 2023, [@tseignette](https://gitlab.com/tseignette) rejoint le projet Terrecole et contribue à ce journal à mesure qu'il contribue au site Kirby.

Ce journal de bord est grandement inspiré des explorations faites par [David Larlet](https://larlet.fr) et partagées dans son article [**Traces**](https://larlet.fr/david/2022/12/19/).

## 23 février 2024

### @tseignette

-   Je rajoute la sélection de plusieurs écoles par article.

⏰ _crédits consommé : 0.5_

## 16 février 2024

### @tseignette

-   Au fur et à mesure depuis la dernière fois : j'ai relu le travail de Thibaud.
-   On s'appelle avec Thibaud pour relire et corriger en même temps.
-   Je termine et cloture les dernières demandes de fusion : les filtres, l'affichage des actualités sur les écoles, le remplacement de `node`.

⏰ _crédits consommé : 6_

## 11 février 2024

### @mlbiche

-   🍄 Je mets à jour les dépendances du projet.
-   🏫 Je mets en place l'affichage des articles de la catégorie Collège dans la page du collège.
-   🏫 Je mets en place l'affichage des articles associés à une école dans la page de l'école.
-   🏫 J'ajoute le filtre par école.

⏰ _crédits consommés : 2 / motivation : 5_

## 9 février 2024

### @mlbiche

-   📖 Relecture et correction de la fonctionnalité des filtres à 4 mains avec Thomas.

⏰ _crédits consommés : 0,75 / motivation : 5_

## 2 février 2024

### @mlbiche

-   🥇 Je corrige la hiérarchie des titres de la page d'accueil pour améliorer l'accessibilité et la SEO.
-   🗞 Je commence à travailler sur l'affichage des articles liés au collège et aux écoles sur les pages respectives des établissements.

⏰ _crédits consommés : 0,5 / motivation : 4_

## 1er février 2024

### @mlbiche

-   🏷 J'ajoute le filtre par catégorie sur les rubriques.

⏰ _crédits consommés : 2 / motivation : 5_

## 29 janvier 2024

### @mlbiche

-   📖 Je relis et valide le travail de Thomas sur l'optimisation des images.
-   🏷 Je modifie les catégories d'article pour qu'on puisse en sélectionner plusieurs.
-   🏷 J'ajoute la possibilité de n'afficher que les articles d'une catégorie dans une rubrique en fonction d'une catégorie passée en paramètre dans l'URL.

⏰ _crédits consommés : 0,5 / motivation : 5_

## 26 janvier 2024

### @tseignette

-   Tout le long de janvier : j'ai relu le travail de Thibaud.
-   J'ai corrigé les images pas toujours centrées sur la page d'un article.
-   J'ai ajouté l'optimisation des images. À titre d'exemple, le poids de [cet article](https://terrecole.fr/actualites/hifek) passe de 1,8Mo à ~550ko, soit une réduction par plus de 3 !

⏰ _crédits consommé : 4_

## 23 janvier 2024

### @mlbiche

-   🏫 Je rajoute la possibilité de lier un article à une école.

⏰ _crédits consommés : 1 / motivation : 5_

## 17, 18 et 19 janvier 2024

### @mlbiche

-   🏓 Je prends en compte les retours de Thomas sur les mentions légales, la réécriture des rubriques et les écoles.

⏰ _crédits consommés : 1,25 / motivation : 4_

## 16 janvier 2024

### @mlbiche

-   🏷 Je commence à mettre en place l’étiquetage des articles des rubriques par catégorie.

⏰ _crédits consommés : 0,75 / motivation : 5_

## 15 janvier 2024

### @mlbiche

-   🧃 Je réécris la brique logicielle des rubriques sans passer par un plugin. Cette solution s'avère être trop complexe pour ce qu'elle faisait. On peut facilement associer différentes pages à plusieurs _blueprints_ et _templates_ en nommant de la même manière que le _blueprint_ et le _template_ le fichier du dossier `content`.

⏰ _crédits consommés : 0,5 / motivation : 5_

## 12 janvier 2023

### @mlbiche

-   📜 J'ajoute la page des mentions légales. On avait annoncé ~XS. Ce n'est en fait pas si simple parce qu'on n'avait pour le moment juste pas prévu comment afficher un champ de texte libre.
-   🐛 Je corrige un bug qui empêchait de publier la page **Le collège** si des horaires pour le samedi et le dimanche n'étaient pas fournis.
-   🏫 Je prépare la page **Les écoles** et ses sous-pages.

⏰ _crédits consommés : 3,5 / motivation : 5_

## 29 décembre 2023

### @mlbiche

-   🔭 Je rajoute une page d'erreur.

⏰ _crédits consommés : 0,5 / motivation : 3_

## 27 décembre 2023

### @mlbiche

-   📄 Je rédige la documentation du projet pour faciliter la prise en main et les contributions _open-source_.

⏰ _crédits consommés : 0,5 / motivation : 5_

## 15 décembre 2023

### @mlbiche

-   🛋 Je relis le travail de Thomas sur la page Collège.

⏰ _crédits consommés : 0,25 / motivation : 5_

## 13 décembre 2023

### @mlbiche

-   🛬 Je paire avec Thomas pour finaliser la nouvelle page d'accueil.
-   🚀 Je mets en production la nouvelle page d'accueil.

⏰ _crédits consommés : 1,25 / motivation : 5_

### @tseignette

-   Appel avec Thibaud pour passer en revue les retours sur la page d'accueil
-   Finition de la page "Le collège" suite aux retours de Thibaud
-   Ajustements techniques, amélioration du style de la rubrique dans un article

⏰ _crédits consommé : 2_

## 12 décembre 2023

### @mlbiche

-   🎨 Je termine d'appliquer le nouveau coup de peinture sur la page d'accueil.
-   🛰 Je rends visible le survol des cartes d'article.

⏰ _crédits consommés : 1,75 / motivation : 5_

### @tseignette

-   Relecture du travail de Thibaud sur le nouveau style de l'accueil
-   Mise au propre de l'ébauche de la nouvelle page "Le collège" suite à l'appel de la semaine précédente avec Émilie. Il reste quelques bricoles techniques, j'attends que Thibaud jette un coup d'œil avant de terminer.

⏰ _crédits consommé : 3_

## 8 décembre 2023

### @mlbiche

-   🛋 Je relis le travail réalisé par Thomas ces jours-ci.
-   🔧 J'applique quelques correctifs mineurs suite aux retours faits à Thomas. Je paire avec Thomas pour améliorer quelques morceaux de code. On met en place des modèles Kirby. ([8f9c72ab](https://gitlab.com/terrecole/terrecole/-/merge_requests/18/diffs?commit_id=8f9c72abcf5f4aa9c224d651de3b39ff7f27824a))
-   🍄 Je mets à jour la liste des compatibilités de navigateur. L'alerte apparaissait lors de la construction des styles, mais pas assez visible. J'en profite pour mettre à jour toutes les dépendances du projet. Les Internets vont trop vite...
-   🏚 Je remplace un style qui pourrait entraîner des incompatibilités sur certains navigateurs. ([CanIUse](https://caniuse.com/mdn-css_properties_align-items_flex_context_start_end))
-   🦷 Je commence à reprendre la page d'accueil. Je me casse complètement les dents avec les SVG. Un coup pour un `s` manquant sur un `module.export`, un autre coup à cause d'un `:` devant `xlink:href` à cause d'un copier-coller. Les deux fois, je passe beaucoup de temps à debugger des choses qui fonctionnent...

⏰ _crédits consommés : 4 / motivation : 3_

## 7 décembre 2023

### @tseignette

-   Discussion avec Thibaud sur la manière d'aborder les libellés de la pagination et sur les prochaines tâches
-   Dernière passe sur la nouvelle rubrique "La SCIC Terrecole"
-   Amélioration de l'accessibilité et du style de la carte article
-   Correction du style et des voisins (suivant / précédent) d'un article sans date
-   Ébauche de la nouvelle page "Le collège" pour préparer l'appel du lendemain

⏰ _crédits consommé : 4_

## 6 décembre 2023

### @tseignette

-   Finalisation de la nouvelle rubrique "La SCIC Terrecole" suite aux réponses d'Émilie et aux retours de Thibaud, une dernière passe et ça devrait être bon. Il a fallu tester plusieurs solutions avant d'arriver à quelque chose de correct pour séparer les rubriques avec ou sans date. Ça m'a pris plus de temps que ce que je pensais, au final on s'est pas trompé sur l'estimation !

⏰ _crédits consommé : 3_

## 1 décembre 2023

### @tseignette

-   installation de pnpm, lancement du projet : super rapide, vive Kirby
-   passage en revue des tâches pour noter quelques idées
-   fonctionnalité : Ajouter une page de type rubrique "La SCIC Terrecole"
-   ajout du journal de bord sur le dépôt

⏰ _crédits consommé : 3_

## 13 novembre 2023 - 24 novembre 2023

-   💽 (tech) Je corrige un problème qui empêche de générer des fichiers de style _minifiés_.
-   🖌️ Je change l’affichage du texte des boutons primaires actifs en blanc.
-   💅 (accueil,projet,actualites) J’essaie un nouveau design des cartes d'article dans les différentes rubriques.
-   📜 (accueil) Je commence et itère l’ajout du manifeste. Je fais le point avec Émilie plusieurs points. J’échange avec Léa qui doit me faire une proposition.
-   🤝 (accueil) Je travaille sur le bandeau de partenariat.
-   👋 (accueil) J’ajoute le bandeau de contact.
-   ✍️ (accueil) Je mets en place la personnalisation du contenu du manifeste

⏰ _crédits consommés : 9,5 / motivation : 5_

## 6 novembre 2023 - 10 novembre 2023

-   🧽 (article) Je retire un bloc de description qui ne devrait pas apparaître s'il est vide.
-   🐞 (accueil) Je corrige un bug de compatibilité qui ne centre pas les titres des pages sur les navigateurs chromium.
-   🛠 (tech) Je corrige la construction des styles en développement.
-   🪲 (accueil,article) Je corrige l'apparition d'un espace blanc au-dessus des titres sur des écrans à grande résolution.
-   ✏️ (projet) Je renomme les articles de la rubrique projet en ressource.
-   🤓 (rubrique,article) J'utilise des collections pour alléger le code.
-   📚 (accueil) J'ajoute des rubriques contenant les dernières ressources et les derniers articles.
-   🚧 (accueil) Je retire l'option en construction du panneau d'administration.
-   👓 (article) J'améliore la présentation et la syntaxe dans plusieurs articles.
-   🐜 (article) Je corrige l'affichage de l’enchaînement de blocs de liste puis de texte.
-   ⏫ (admin) J'affiche les derniers articles en premier.

⏰ _crédits consommés : 4,75 / motivation : 4_

## 16 octobre 2023 - 20 octobre 2023

-   📑 Je rajoute une icône de document sur les liens vers le Google Drive.
-   📏 Je corrige le débordement des liens sur petit écran (iPhone 5/SE 1ère génération notamment) pour qu’il s’affiche sur plusieurs lignes. Cette mesure rejoint la démarche d’éco-conception (pas d’incitation au renouvellement du matériel). 🌱
-   ⏭️ Je rajoute des liens pour naviguer entre les articles.
-   🪃 J’ajoute au texte de site en construction des liens vers les rubriques **Le projet** et **Nos actualités**.
-   🔗 Je rend visible l’aspect _cliquable_ du lien dans le bloc de lien dans le panneau d’administration.
-   ➿ Je rends optionnelle la description d’un article.
-   📃 Je mets en place la pagination sur les rubriques **Le projet** et **Nos actualités**.
-   🎙️ Je prépare le texte sur la démarche derrière le site Terrecole.

⏰ _crédits consommés : 4,75 / motivation : 5_

## 9 octobre 2023 - 13 octobre 2023

-   🔐 Je corrige des problèmes d’accès au site en 4G en raison de certificat SSL (HTTPS) indisponible en IPv6, privilégiée par les réseaux mobiles (histoire tarabiscotée comprise sur un coup de chance).

⏰ _crédits consommés : 1 / motivation : 4_

## 2 octobre 2023 - 6 octobre 2023

-   💅 Je finalise la mise en forme des articles.
-   🍺 Je mets en place un environnement de pré-production sur https://en-fermentation.terrecole.fr/.
-   🎊 Je présente l’interface d’administration à Émilie et Chris, ainsi que la publication d’articles.
-   🐛 Je corrige un bug d’affichage des blocs titre dans les articles.
-   🪲 J’empêche de post-dater un article.
-   🐜 J’empêche de publier un article avec l’état `Public`.
-   🐝 Je corrige le fonctionnement du menu “burger” en version mobile.
-   🛋️ Je relis le travail réalisé jusqu’à maintenant sur la partie Actualités.
-   🚀 J’envoie en production toutes les petites nouveautés.
-   🖌️ Je remplace le titre de la page d’accueil par le logo de Terrecole.
-   🐞 J’ajoute la page d’accueil au plan du site.
-   📽️ J’ajoute la page **Le projet** sur la même base que la page **Nos actualités**. Je me retrouve encore une fois un peu contraint pour mettre en place le design par le fait que les couleurs secondaire (jaune) et tertiaire (bleu clair) ne sont pas accessibles sur du blanc ou la couleur quaternaire (beige).
-   🔗 J’ajoute le bloc de type lien.

⏰ _crédits consommés : 7,75 / motivation : 5_

## 25 septembre 2023 - 29 septembre 2023

-   📝 Je finalise la mise en forme de la page **Nos actualités** et avance sur le squelette des articles.

⏰ _crédits consommés : 2,25 / motivation : 5_

## 18 septembre 2023 - 22 septembre 2023

-   🤖 Je me galère pour changer un peu la façon dont les fichiers de style _(à quoi ressemble le bazar)_ et les scripts _(comment le bazar régit quand l’utilisateur interagit)_ sont construits pour chaque page. Mais résultat, chaque page se retrouve allégée !
-   🗒 Je commence à mettre en forme le squelette des articles.
-   🖌️ Je reprends pas mal d’éléments de design sur l’en-tête et la barre de navigation, le pied de page. Ça affecte la page d’accueil, mais aussi ça reprend la page **Nos actualités**. Je fais une tentative de nouvelle interface des cartes des articles de la page **Nos actualités.**

⏰ _crédits consommés : 4,75 / motivation : 4_

## 11 septembre 2023 - 15 septembre 2023

-   🗒 J’avance pas mal sur la section **Actualités**. Je patauge un peu pour inventer une UI jolie, mais prend pas mal d’inspiration du côté du Low Tech Lab.

⏰ _crédits consommés : 3,25 / motivation : 5_

## 4 septembre 2023 - 8 septembre 2023

-   🗒 Je m’attaque à la mise en place de la partie **Actualités** du site.

⏰ _crédits consommés : 1 / motivation : 4_

## 24 juillet 2023 - 20 juin 2023

-   🕵️ Je rajoute l’outil de suivi Matomo.

⏰ _crédits consommés : 1,5 / motivation : 5_

## 18 juillet 2023

-   🤖 Je rajoute le fichier `robots.txt` qui permet aux robots des moteurs de recherche d’indexer efficacement le site (améliore la SEO).

⏰ _crédits consommés : 0,5 / motivation : 3_

## 13 juillet 2023

-   🖼 Je rajoute le favicon du site, compatible avec toutes les plateformes. C’est pénible de générer l’image dans les bons formats, donc je n’intègre pas la personnalisation du favicon dans le panneau d’administration. On verra dans un futur plus ou moins loin si le besoin existe.

⏰ _crédits consommés : 2 / motivation : 4_

## 6 juillet 2023 - 7 juillet 2023

-   💅 Je rajoute le logo et la personnalisation du logo. Je rajoute les couleurs du projet et les typographies.
-   🤖 Je finalise le travail sur les SEO (performance de la page dans les résultats des moteurs de recherche) et les métadonnées pour le partage du site sur les réseaux sociaux.
-   🧪 Je mets en place la validation automatique du code du projet.

⏰ _crédits consommés : 2 / motivation : 4_

## 30 juin 2023

-   🔩 J’essaie de mettre en place la synchronisation des changements appliqués depuis le panneau d’administration avec le dépôt de _versionnage_ de code. Je n’arrive pas à mes fins… Mais on doit largement pouvoir s’en passer, au moins pendant un temps.
-   🤖 Je commence à travailler sur l’amélioration du SEO (performance de la page dans les résultats des moteurs de recherche).

⏰ _crédits consommés : ~~3~~ 1,25 / motivation : 2_

## 20 juin 2023 - 23 juin 2023

-   📲 Je fais appel à Thomas qui m’épaule pour l’hébergement web sur Scaleway. On règle le sujet en un rien de temps, avec signature des certificats pour le HTTPS et tout le bazar.
-   🚀 [Terrecole | Accueil](http://terrecole.fr/)

⏰ _crédits consommés : 1 / motivation : 5_

## 15 juin 2023

-   🪃 Je mets en place la redirection de tous les autres domaines vers le domaine principal terrecole.fr.
-   💩 Je patine toute une journée comme un **_idiot_** pour essayer de déployer “rapidement” le site sur mon propre serveur Gandi pour pouvoir faire une démo. Impossible de signer les certificats pour avoir le HTTPS _(👨‍💻 pour cause d’une IPv6 qui ne répond pas correctement ?)_, puis impossible de faire fonctionner le site sur le serveur _(👨‍💻 pour cause d’installation de PHP 8.2 fastidieuse et de configuration nginx avec Kirby impossible)_. Horrible ! Heureusement qu’il y a la fête du porc en perspective. 🐖
-   💳 Je craque en fin de journée et commande une offre d’hébergement web Scaleway pour voir comment ça se passe.

⏰ _crédits consommés : ~~6~~ 1,75 / motivation : -1_

## 30 mai 2023 - 2 juin 2023

-   🚧 Je prépare la page de site en construction. Ça me permet de bricoler un peu avec Bulma et Kirby. J’essaie la semaine prochaine de déployer ça sur le domaine de Terrecole.

⏰ _crédits consommés : 1,75 / motivation : 5_

## 15 mai 2023 - 19 mai 2023

-   🤝 Je prends en main [Kirby](https://getkirby.com/), j’explore un peu via [Racines de résilience](https://github.com/Terractiva/racines-de-resilience) et le kit de démarrage de Kirby. Thomas répond à mes questions et m’aiguille un peu. Assez enthousiaste et plus confiant sur la prise en main. Je comprends l’idée générale : Kirby ne pré-mâche aucun travail et permet un maximum de personnalisation du tableau de bord d’administration. Donc tout peut le contenu peut être administré.
-   🧪 Je prépare tout l’environnement de développement, jusqu’à la préparation des scripts et des styles.
-   💅 Je rajoute [Bulma CSS](bulma.io/).
-   💆‍♂️🦶 Je prépare l’en-tête et le pied de page pour expérimenter un peu. J’ajoute la personnalisation du contenu du pied de page.
-   🧭 Je compare les offres des différents hébergeurs Web _“bio”_. [Scaleway](https://www.scaleway.com/fr/) semble une bonne option bio et moderne (utilisé par le marché beta.gouv.fr).

⏰ _crédits consommés : 6,5 / motivation : 5_
